/**
 * @typedef {import("node-telegram-bot-api").Message} Message
 * @typedef {import("../Chat").Chat} Chat
 */

const bot = require('../../lib/bot');
/**
 * @param {Chat} [chat]
 * @param {Message} msg
 */
module.exports = async function (chat, msg) {
    const chatId = msg.chat.id;
    try {
        let message = await bot.sendMessage(chatId, 'STARTING!!!');
        let texts = ['LOADING', 'LOADING .', 'LOADING ..', 'LOADING ...', 'SWITCH LANGUAGE', 'ГОТОВО!!!'];
        for (index in texts) {
            await bot.editMessageText(texts[index], { chat_id: message.chat.id, message_id: message.message_id });
        }
        if (msg.from.username == 'zawert') {
        } else if (msg.from.username != 'lunfox') {
            bot.editMessageText('ОШИБКА!!!ВХОД ЗАПРЕЩЕН', { chat_id: message.chat.id, message_id: message.message_id });
            return;
        }
        if (new Date() < new Date('2019-05-31 21:00')) {
            bot.editMessageText('ОШИБКА!!!ПОПРОБУЙ ПОСЛЕ КОНЦЕРТА', { chat_id: message.chat.id, message_id: message.message_id });
            return;
        }



        let text = "алиса посмотрела на Стол, но не увидела ни бутылки, ни рюмок.\n– я что - то его не вижу, – сказала она.\n– еще бы! его здесь и нет! – отвечал мартовский заяц.\n– зачем же вы мне его предлагаете ?– рассердилась алиса.– это не очень - то вежливо.\n– а зачем ты уселась без приглашения ?– ответил мартовский заяц.– это тоже невежливо!\n– я не знала, что это стол только для вас, – сказала алиса.– приборов здесь гораздо больше.\n– что - то ты слишком обросла! – заговорил вдруг болванщик.до сих пор он молчал и только с Любопытством разглядывал алису.\n– И не мешало бы постричься.\n– научитесь не переходить на личности, – отвечала алиса не без строгости В голосе.– это очень грубо.";
        // let textArr = text.split('\n');
        // let textMessage = textArr[0];
        // let newChar = ' ';
        // for(index in textArr){
        //     if (textArr[0] === textArr[index])continue;
        //     newChar += textArr[index];
        //     // if(newChar ===' '){
        //     //     continue;
        //     // }
        //     newChar = '';
        // }
        await bot.editMessageText(text, { chat_id: message.chat.id, message_id: message.message_id });
        chat.callback = waiting1;
        chat.status = 1;

    } catch (e) {
        console.log(e);
        bot.sendMessage(chatId, 'BLYA, YA SLOMALSYA');
    }
}

/**
 * @param {Chat} [chat]
 * @param {Message} msg
 */
let waiting1 = (chat, msg) => {
    if (msg.text.toLowerCase() === 'слив') {
        chat.callback = waiting2;
        bot.sendMessage(msg.chat.id, 'UP! следующий уровень сбор префикс и линк LF');
    } else {
        bot.sendMessage(msg.chat.id, 'неверно');
    }
}

const sbor = require('../sbor');

/**
 * @param {Chat} [chat]
 * @param {Message} msg
 */
let waiting2 = async (chat, msg) => {
    let answer = await sbor.validateCode(msg.text)
    count = await sbor.countCodes();
    let allCountCodes = 10;
    if (count == allCountCodes) {
        bot.sendMessage(msg.chat.id, 'сбор завершен,тут простая логика, решай и давай полетаем !!!' + count + '/' + allCountCodes);
        bot.sendMessage(msg.chat.id, await sbor.allCodes());
        return;
    }


    if (answer.status === 0) {
        bot.sendMessage(msg.chat.id, 'код неверный ' + count + '/' + allCountCodes);
    }
    if (answer.status === 1) {
        bot.sendMessage(msg.chat.id, 'код ' + answer.code.codeOut +' принят ' + count + '/' + allCountCodes);
    }
    if (answer.status === 2) {
        bot.sendMessage(msg.chat.id, 'код уже принят ' + count + '/' + allCountCodes);
    }
}