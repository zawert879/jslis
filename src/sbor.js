const db = require('../lib/db');
module.exports.validateCode =  async searchCode => {
    let answer = 0;
    code = await db.lis.findOne({ code: searchCode });
    if (code === null) {answer = 0;}
    else if (code.status === true){ answer = 2;}
    else if (code.status === false) {
        code.status = true;
        db.lis.update({ _id: code._id }, code);
        answer = 1;
    }
    return {status:answer,code:code};
}

module.exports.countCodes = async searchCode => {
    codes = await db.lis.find({status : true});
    return codes.length ; 
}

module.exports.allCodes = async searchCode => {
    codes = await db.lis.find({});
    return codes.map(item=>{
        return item.codeOut;
    }).join(', ');
}

