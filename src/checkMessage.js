const log = require('../lib/log')
const bot = require('../lib/bot')
const Chat = require('./Chat');

let commands = [];
const normalizedPath = require("path").join(__dirname, "commands");
require("fs").readdirSync(normalizedPath).forEach(function (file) {
    commands['/'+file.replace('.js', '')] = require('./commands/' + file);
});

bot.on('message', msg => {
    let chat = Chat.firthOrCreate(msg.chat.id,msg.from.id);
    if(msg.text == '/back'){
        chat.status = 0;
        bot.sendMessage(msg.chat.id,'ok');
    }
    if (chat.status === 0) {
        if (commands[msg.text]){
            commands[msg.text](chat,msg);
            }
    }else{
        chat.callback(chat,msg);
    }
})