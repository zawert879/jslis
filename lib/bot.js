let TelegramBot = require('node-telegram-bot-api');
let config = require('../config/telegram.json');

const TOKEN = config.Token;
const PROXY = config.Proxy;
module.exports = new TelegramBot(TOKEN, {
    polling: true,
    request: {
        proxy: PROXY.type + "://" + PROXY.ip + ":" + PROXY.port + "/"
    }
});

console.log('start');